#include <stdio.h>
#include "header.h"

void inserir(){
    struct aluno p;
    struct notas n;

    FILE *faluno = fopen("aluno.txt","r");
    FILE *fnotas = fopen("notas.txt","w");

    if(fnotas == NULL || faluno == NULL){
        printf("Erro ao abrir o arquivo");
    } else {
        while(fread(&p, sizeof(p), 1, faluno) == 1){
            n.ra = p.ra;
            printf("Notas para o aluno %s\n",p.nome);
            printf("Digite a primeira nota: ");
            scanf("%f",&n.n1);
            fflush(stdin);
            printf("Digite a segunda nota: ");
            scanf("%f",&n.n2);
            n.aprovado = (n.n1 >= 6 || n.n2 >= 6) ? 1 : 0;
            fwrite(&n, sizeof(n), 1, fnotas);
        }

        fclose(fnotas);
        fclose(faluno);
    }
}