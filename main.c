#include <stdio.h>
#include "header.h"

int main() {
    int r;
    do {
        printf("Bem vindo as notas de alunos\n");
        printf("1. Cadastrar Aluno\n");
        printf("2. Inserir Conceitos (Notas) de Disciplinas\n");
        printf("3. Exibir Todas as Notas e Média por Disciplina de um aluno\n");
        printf("4. Exibir alunos aprovados em Todas as Disciplinas\n");
        printf("5. Exibir alunos reprovados e informar em quais Disciplinas\n");
        printf("6. Sair do programa\n");
        scanf("%d", &r);
        switch (r) {
            case 1:
                cadastrar();
                break;
            case 2:
                inserir();
                break;
            case 3:
                exibe();
                break;
            case 4:
                aprovados();
                break;
            case 5:
                reprovados();
                break;
            case 6:
                printf("bye bye");
                break;
            default:
                printf("Escolha indefinida");
        }
    } while(r != 6);

    return 0;
}