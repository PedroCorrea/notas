#ifndef NOTAS_HEADER_H
#define NOTAS_HEADER_H

struct aluno{
    int ra;
    char nome[10];
};

struct notas{
    int ra;
    float n1;
    float n2;
    int aprovado;
};

void inserir();
void cadastrar();
void exibe();
void reprovados();
void aprovados();

#endif //NOTAS_HEADER_H
