#include <stdio.h>
#include "header.h"

void cadastrar(){
    struct aluno p;
    int r;

    FILE *f = fopen("aluno.txt","a");
    if(f == NULL){
        printf("Erro ao abrir o arquivo");
    } else {
        do {
            printf("Digite o RA do aluno: ");
            scanf("%d", &p.ra);
            fflush(stdin);
            printf("Digite o nome do aluno: ");
            scanf("%s", &p.nome);
            fflush(stdin);
            fwrite(&p, sizeof(p), 1, f);
            printf("Deseja cadastrar outra pessoa? ");
            scanf("%d", &r);
        } while(r != 0);

        fclose(f);
    }

}
