#include <stdio.h>
#include "header.h"
struct aluno p;
struct notas n;

void exibe(){
    FILE *faluno = fopen("aluno.txt","r");
    FILE *fnotas = fopen("notas.txt","r");

    if(fnotas == NULL || faluno == NULL){
        printf("Erro ao abrir o arquivo");
    } else {
        while((fread(&p, sizeof(p), 1, faluno) == 1) && (fread(&n, sizeof(n), 1, fnotas))){
            if(p.ra == n.ra) {
                //printf("%d = %d\n",p.ra,n.ra);
                printf("Aluno: %s\n", p.nome);
                printf("Nota 1: %.2f\n",n.n1);
                printf("Nota 2: %.2f\n",n.n2);
                //printf("Media: %.2f\n",n.media);
                printf((n.aprovado == 1) ? "Aprovado" : "Reprovado");
                printf("\n");
            }
        }
        getchar();
        printf("\n\n");
    }
}

void aprovados(){
    FILE *faluno = fopen("aluno.txt","r");
    FILE *fnotas = fopen("notas.txt","r");

    if(fnotas == NULL || faluno == NULL){
        printf("Erro ao abrir o arquivo");
    } else {
        while((fread(&p, sizeof(p), 1, faluno) == 1) && (fread(&n, sizeof(n), 1, fnotas))){
            if(p.ra == n.ra && n.aprovado == 1) {
                //printf("%d = %d\n",p.ra,n.ra);
                printf("Aluno: %s\n", p.nome);
                printf("N1: %.2f\n",n.n1);
                printf("N2: %.2f\n",n.n2);
                printf("\n");
            }
        }
        getchar();
        printf("\n\n");
    }
}

void reprovados(){
    FILE *faluno = fopen("aluno.txt","r");
    FILE *fnotas = fopen("notas.txt","r");

    if(fnotas == NULL || faluno == NULL){
        printf("Erro ao abrir o arquivo");
    } else {
        while((fread(&p, sizeof(p), 1, faluno) == 1) && (fread(&n, sizeof(n), 1, fnotas))){
            if(p.ra == n.ra && n.aprovado == 0) {
                //printf("%d = %d\n",p.ra,n.ra);
                printf("Aluno: %s\n", p.nome);
                if(n.n1 < 6) printf("Reprovou na materia N1: %.2f\n",n.n1);
                if(n.n2 < 6) printf("Reprovou na materia N2: %.2f\n",n.n2);
                printf("\n");
            }
        }
        getchar();
        printf("\n\n");
    }
}